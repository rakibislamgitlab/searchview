package com.bitm.listviewandrecyclerviewmad14;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MovieRVAdapter extends RecyclerView.Adapter<MovieRVAdapter.MovieViewHolder>
implements Filterable{
    private static final String TAG = MovieRVAdapter.class.getSimpleName();
    private Context context;
    private List<Movie> movieList;
    private List<Movie> filteredMovieList;
    private int count = 0;

    public MovieRVAdapter(Context context, List<Movie> movieList) {
        this.context = context;
        this.movieList = movieList;
        filteredMovieList = movieList;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //step 1
        count++;
        Log.e(TAG, "onCreateViewHolder called "+count+" times");
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.movie_row,
                        parent, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        //step 3
        holder.nameTV.setText(filteredMovieList.get(position).getName());
        holder.yearTV.setText(
                String.valueOf(filteredMovieList.get(position)
                .getReleaseYear())
        );
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(context, "features coming soon..", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredMovieList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String query = charSequence.toString();
                if (query.isEmpty()){
                    filteredMovieList = movieList;
                }else{
                    List<Movie> tempList = new ArrayList<>();
                    for (Movie m : movieList){
                        if (m.getName().toLowerCase().contains(query.toLowerCase()) ||
                        String.valueOf(m.getReleaseYear()).contains(query))
                        {
                            tempList.add(m);
                        }
                    }
                    filteredMovieList = tempList;
                }
                FilterResults results = new FilterResults();
                results.values = filteredMovieList;
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredMovieList = (List<Movie>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class MovieViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV, yearTV;
        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            //step 2
            nameTV = itemView.findViewById(R.id.row_name);
            yearTV = itemView.findViewById(R.id.row_year);
        }
    }

    public void updateList(List<Movie> movies){
        this.movieList = movies;
        this.filteredMovieList = movies;
        notifyDataSetChanged();
    }
}
