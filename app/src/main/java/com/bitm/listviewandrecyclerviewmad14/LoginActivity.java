package com.bitm.listviewandrecyclerviewmad14;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {
    private AdminAuthPreference preference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preference = new AdminAuthPreference(this);
        if (preference.getLoginStatus()){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
        setContentView(R.layout.activity_login);
        setTitle("Admin Login");
    }

    public void loginAdmin(View view) {
        final TextInputEditText emailET = findViewById(R.id.emailInput);
        final TextInputEditText passET = findViewById(R.id.passwordInput);
        final String email = emailET.getText().toString();
        final String pass = passET.getText().toString();
        if (email.isEmpty()){
            emailET.setError("This field must not be empty");
            return;
        }
        if (pass.isEmpty()){
            passET.setError("This field must not be empty");
            return;
        }

        if (email.equals("admin@bitm.com") && pass.equals("123456")){
            preference.setLoginStatus(true);
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }else{
            Toast.makeText(this, "Invalid email or password", Toast.LENGTH_SHORT).show();
        }

    }
}
