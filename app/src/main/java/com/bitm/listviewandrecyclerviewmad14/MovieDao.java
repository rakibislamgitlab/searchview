package com.bitm.listviewandrecyclerviewmad14;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface MovieDao {
    @Insert
    long insertNewMovie(Movie movie);

    @Update
    int updateMovie(Movie movie);

    @Delete
    int deleteMovie(Movie movie);

    @Query("select * from Movie order by movieId desc")
    List<Movie> getAllMovies();
}
