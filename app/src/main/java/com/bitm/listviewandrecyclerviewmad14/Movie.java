package com.bitm.listviewandrecyclerviewmad14;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Movie {
    @PrimaryKey(autoGenerate = true)
    private int movieId;
    private int movieImage;
    private String name;
    private String category;
    private int releaseYear;

    public Movie(String name, String category, int releaseYear) {
        this.name = name;
        this.category = category;
        this.releaseYear = releaseYear;
    }

    @Ignore
    public Movie(int movieId, String name, String category, int releaseYear) {
        this.movieId = movieId;
        this.name = name;
        this.category = category;
        this.releaseYear = releaseYear;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getMovieImage() {
        return movieImage;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setMovieImage(int movieImage) {
        this.movieImage = movieImage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }
}
