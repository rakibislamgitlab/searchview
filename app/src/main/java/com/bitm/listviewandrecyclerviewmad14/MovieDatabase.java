package com.bitm.listviewandrecyclerviewmad14;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Movie.class}, version = 1)
public abstract class MovieDatabase extends RoomDatabase {
    private static MovieDatabase db;
    public abstract MovieDao getMovieDao();
    public static MovieDatabase getInstance(Context context){
        if (db != null){
            return db;
        }

        db = Room.databaseBuilder(context, MovieDatabase.class,
                "movie_db")
                .allowMainThreadQueries()
                .build();
        return db;
    }
}
