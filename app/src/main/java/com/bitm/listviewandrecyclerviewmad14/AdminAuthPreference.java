package com.bitm.listviewandrecyclerviewmad14;

import android.content.Context;
import android.content.SharedPreferences;

public class AdminAuthPreference {
    public static final String IS_LOGGED_IN = "isLoggedIn";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public AdminAuthPreference(Context context){
        preferences = context.getSharedPreferences("admin_pref",Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setLoginStatus(boolean status){
        editor.putBoolean(IS_LOGGED_IN, status);
        editor.commit();
    }

    public boolean getLoginStatus(){
        return preferences.getBoolean(IS_LOGGED_IN, false);
    }
}
