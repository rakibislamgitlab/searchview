package com.bitm.listviewandrecyclerviewmad14;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView movieRV;
    private MovieLVAdapter lvAdapter;
    private MovieRVAdapter rvAdapter;
    private AdminAuthPreference preference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Movie List");
        preference = new AdminAuthPreference(this);
        movieRV = findViewById(R.id.movieLV);
        List<Movie> movies =
                MovieDatabase.getInstance(this)
                .getMovieDao()
                .getAllMovies();
        //lvAdapter = new MovieLVAdapter(this, movieList);
        rvAdapter = new MovieRVAdapter(this, movies);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        //llm.setOrientation(RecyclerView.HORIZONTAL);
        //GridLayoutManager glm = new GridLayoutManager(this, 2);
        movieRV.setLayoutManager(llm);
        movieRV.setAdapter(rvAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.item_search)
                .getActionView();
        searchView.setQueryHint("search by movie name or year");
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                rvAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_add:
                showAddMovieDialog();
                break;
            case R.id.item_logout:
                preference.setLoginStatus(false);
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddMovieDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.add_move_dialog, null);

        final EditText nameET = view.findViewById(R.id.nameInput);
        final EditText categoryET = view.findViewById(R.id.categoryInput);
        final EditText yearET = view.findViewById(R.id.movieYearInput);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add New Movie");
        builder.setView(view);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String name = nameET.getText().toString();
                String category = categoryET.getText().toString();
                String year = yearET.getText().toString();
                Movie movie = new Movie(name, category, Integer.parseInt(year));
                //insert into table
                long insertedRow =
                        MovieDatabase.getInstance(MainActivity.this)
                                .getMovieDao()
                                .insertNewMovie(movie);
                if (insertedRow > 0){
                    Toast.makeText(MainActivity.this, "Added successfully", Toast.LENGTH_SHORT).show();
                    List<Movie> movies =
                            MovieDatabase.getInstance(MainActivity.this)
                                    .getMovieDao()
                                    .getAllMovies();
                    rvAdapter.updateList(movies);
                }
                //rvAdapter.updateList(movie);

            }
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
