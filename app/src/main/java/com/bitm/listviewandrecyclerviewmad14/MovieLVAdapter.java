package com.bitm.listviewandrecyclerviewmad14;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MovieLVAdapter extends ArrayAdapter<Movie> {
    private Context context;
    private List<Movie> movieList;
    private int count = 0;
    private final String TAG = MovieLVAdapter.class.getSimpleName();

    public MovieLVAdapter(Context context, List<Movie> movieList) {
        super(context, R.layout.movie_row, movieList);
        this.context = context;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        count++;
        Log.e(TAG, "getView called : "+count+" times");
        //step 1 - inflate the layout file
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.movie_row, parent, false);
        //step 2 - initialize views
        TextView movieNameTV = convertView.findViewById(R.id.row_name);
        TextView yearTV = convertView.findViewById(R.id.row_year);
        //step 3 - set data
        movieNameTV.setText(movieList.get(position).getName());
        yearTV.setText(String.valueOf(movieList.get(position).getReleaseYear()));

        return convertView;
    }

    public void updateList(Movie movie){
        movieList.add(movie);
        notifyDataSetChanged();
    }
}
